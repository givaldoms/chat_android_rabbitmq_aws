package br.net.sd.chat.chat

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import br.net.sd.chat.R
import br.net.sd.chat.entity.Message
import br.net.sd.chat.entity.User
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton

class ChatAdapter(private val messages: MutableList<Message>,
                  private val user: User,
                  private val mContext: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val self = 0
    private val other = 1

    override fun getItemViewType(position: Int): Int {

        if (messages[position].isFromGroup()) {
            val a = messages[position].sender.split("/")[1]
            println("==========================================\n" + user.username)
            if (user.username == a) {
                return self
            }
            return other

        } else {
            if (messages[position].sender == user.username) {
                return self
            }
            return other
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (holder == null) return
        val message = messages[position]

        when (holder.itemViewType) {
            self -> {
                val h0 = holder as MainViewHolderSelf
                configureViewHolderSelf(h0, message)
            }

            other -> {
                val h0 = holder as MainViewHolderOther
                configureViewHolderOther(h0, message)
            }
        }
    }

    private fun configureViewHolderSelf(h0: MainViewHolderSelf, message: Message) {
        h0.message.text = message.messageContent.body
        h0.timeTextView.text = "${message.date} às ${message.time}"

    }

    private fun configureViewHolderOther(h0: MainViewHolderOther, message: Message) {
        if (message.isFromGroup()) {
            h0.username.text = message.sender

            h0.chatLinearLayout.setOnLongClickListener {
                mContext.alert("Remover usuário do grupo?") {
                    yesButton { }
                    noButton { }
                }
                true
            }

        } else {
            h0.username.visibility = View.GONE
        }

        h0.message.text = message.messageContent.body
        h0.timeTextView.text = "${message.date} às ${message.time}"

    }

    override fun getItemCount() = messages.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder? {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent?.context)

        when (viewType) {
            self -> {
                val v1 = inflater.inflate(R.layout.chat_self_item, parent, false)
                viewHolder = MainViewHolderSelf(v1)
            }

            other -> {
                val v1 = inflater.inflate(R.layout.chat_other_item, parent, false)
                viewHolder = MainViewHolderOther(v1)
            }
        }

        return viewHolder


    }

    class MainViewHolderOther(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val username = itemView.findViewById(R.id.chatUsernameTextView) as TextView
        val message = itemView.findViewById(R.id.chatMessageTextView) as TextView
        val timeTextView = itemView.findViewById(R.id.timeTextView) as TextView
        val chatLinearLayout = itemView.findViewById(R.id.chatLinearLayout) as LinearLayout
    }

    class MainViewHolderSelf(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val message = itemView.findViewById(R.id.chatSelfMessageTextView) as TextView
        val timeTextView = itemView.findViewById(R.id.timeTextView) as TextView

    }

}