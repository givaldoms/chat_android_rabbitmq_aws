package br.net.sd.chat.main

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import br.net.sd.chat.R
import br.net.sd.chat.chat.ChatActivity
import br.net.sd.chat.entity.Conversation
import br.net.sd.chat.entity.User


class MainAdapter(private val chats: MutableList<Conversation>,
                  private val user: User,
                  private val context: Context) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MainViewHolder? {
        val inflater = LayoutInflater.from(parent?.context)
        val v1 = inflater.inflate(R.layout.chat_item, parent, false)
        return MainAdapter.MainViewHolder(v1)
    }

    override fun onBindViewHolder(holder: MainViewHolder?, position: Int) {
        holder?.chatNameTextView?.text = chats[position].name

        if (chats[position].type == 0) {
            holder?.chatTypeImageView?.setImageResource(R.drawable.ic_person_black_24dp)
        } else {
            holder?.chatTypeImageView?.setImageResource(R.drawable.ic_group_black_24dp)

        }

        if (chats[position].hasNotification) {
            holder?.notificationImageView?.visibility = View.VISIBLE
        } else {
            holder?.notificationImageView?.visibility = View.INVISIBLE
        }

        holder?.chatLinearLayout?.setOnClickListener {

            chats[position].hasNotification = false

            val i = Intent(context, ChatActivity::class.java)
            i.putExtra("user", user)
            i.putExtra("conversation", chats[position])
            context.startActivity(i)


        }

    }

    override fun getItemCount() = chats.size


    class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val chatNameTextView = itemView.findViewById(R.id.chatNameTextView) as TextView
        val chatTypeImageView = itemView.findViewById(R.id.chatTypeImageView) as ImageView
        val chatLinearLayout = itemView.findViewById(R.id.chatLinearLayout) as LinearLayout
        val notificationImageView = itemView.findViewById(R.id.notificationImageView) as ImageView

    }
}