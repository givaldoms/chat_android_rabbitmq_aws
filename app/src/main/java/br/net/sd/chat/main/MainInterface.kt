package br.net.sd.chat.main

import br.net.sd.chat.entity.Conversation
import br.net.sd.chat.entity.Message

/**
 * Created by Givaldo Marques
 * on 30/09/2017.
 */


interface MainInteractor {

    interface ConsumerFromQueue {

        fun onNewMessageFromQueue(message: Message)

        fun onNewMessageFromExchange(message: Message)
    }

    fun createQueue(name: String): Boolean

    fun createExchange(exchangeName: String): Boolean

    fun startConsumerFromQueue(queueName: String, consumer: ConsumerFromQueue)

    fun closeConnection()
}

interface MainPresenter {

    fun getConversations(name: String)

    fun createGroup(name: String)

    fun createUser(name: String)

    fun createConversation(name: String, type: Int)

    fun doCloseConnection()

}

interface MainView {

    fun startRecyclerView()

    fun bindAdapter(chats: MutableList<Conversation>)

    fun attRecyclerView()

    fun showAddDialog()

    fun showToast(message: String)

    fun showSnackBar(title: String, conversations: MutableList<Conversation>)

    fun addSwipeAdapterEvent(conversations: MutableList<Conversation>)
}

