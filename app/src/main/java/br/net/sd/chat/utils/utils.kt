package br.net.sd.chat.utils

import android.text.Editable
import android.widget.EditText
import br.net.sd.chat.entity.Message
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*

/**
 * @return a data atual no formato dd/mm/aaaa
 */
fun getDate(): String {
    val sdf = SimpleDateFormat("dd/MM/yyyy", Locale("pt", "BR"))
    return sdf.format(Date())
}

/**
 * @return a hora atual no formato hh:mm
 */
fun getTime(): String {
    val sdf = SimpleDateFormat("hh:mm", Locale("pt", "BR"))
    return sdf.format(Date())
}

/**
 * extensão da classe "String"
 * transforma um objeto de tipo string em um objeto do tipo Mensagem
 * necessário quando recebe a mensagem do servidor
 */
fun String.toMessage(): Message = Gson().fromJson(this, Message::class.java)

fun EditText.clear() {
    this.text = Editable.Factory.getInstance().newEditable("")
}

fun EditText.putText(text: String) {
    this.text = Editable.Factory.getInstance().newEditable(text)
}