package br.net.sd.chat.chat

import br.net.sd.chat.entity.Message

/**
 * Created by Givaldo Marques
 * on 02/10/2017.
 */

interface ChatInteractor {


    interface ConsumerFromQueue {
        fun onNewMessage(message: Message)
    }

    fun startConsumerFromQueue(queueName: String, consumer: ConsumerFromQueue)

    fun sendMessageExchange(exchangeName: String, message: String): Boolean

    fun sendMessageToQueue(queueName: String, message: String): Boolean

    fun addQueueToExchange(queueName: String, exchangeName: String): Boolean

    fun removeQueueToExchange(queueName: String, exchangeName: String): Boolean

    fun closeConnection()


}


interface ChatPresenter {

    fun sendMessage(message: String)

    fun addUserToGroup(username: String, groupName: String)

    fun removeUserToGroup(username: String, groupName: String)

    fun getConversation()

    fun doCloseConnection()

}

interface ChatView {

    fun startRecyclerView()

    fun bindAdapter(chats: MutableList<Message>)

    fun attRecyclerView(sizeList: Int)

    fun showAddDialog()

    fun showToast(message: String)

}
