package br.net.sd.chat.chat

import android.util.Log
import br.net.sd.chat.utils.toMessage
import com.rabbitmq.client.*
import org.jetbrains.anko.doAsync
import java.io.IOException
import java.nio.charset.Charset

/**
 * Created by Givaldo Marques
 * on 02/10/2017.
 */

class ChatInteractorImpl : ChatInteractor {

    private var connection: Connection? = null
    private var channel: Channel? = null
    private var tagConsume = ""

    init {

        doAsync {
            val factory = ConnectionFactory()
            factory.isAutomaticRecoveryEnabled = false
            factory.host = "ec2-52-39-124-26.us-west-2.compute.amazonaws.com"
            factory.username = "administrador"
            factory.password = "sd2017ufs"

            connection = factory.newConnection()
            channel = connection?.createChannel()
        }

    }

    override fun sendMessageExchange(exchangeName: String, message: String): Boolean {

        try {
            doAsync { channel?.basicPublish(exchangeName, "", null, message.toByteArray()) }
        } catch (e: InterruptedException) {
            e.printStackTrace()
            return false
        }
        return true

    }


    /**
     * envia uma mensagem para uma fila selecinada (fila == usuário)
     * @param queueName nome da fila
     * @param message mensagem a ser enviada
     */
    override fun sendMessageToQueue(queueName: String, message: String): Boolean {
        try {
            doAsync { channel?.basicPublish("", queueName, null, message.toByteArray()) }
        } catch (e: IOException) {
            return false
        }
        return true

    }

    /**
     * adiciona usuário ao grupo
     * @param queueName nome do usuário a ser adicionado
     * @param exchangeName nome do grupo a adicionar o usuário
     * @return true se adicionou, false se houve algum erro
     */
    override fun addQueueToExchange(queueName: String, exchangeName: String): Boolean {
        try {
            doAsync { channel?.queueBind(queueName, exchangeName, "") }
        } catch (e: IOException) {
            return false
        }
        return true

    }

    /**
     * remove usuário ao grupo
     * @param queueName nome do usuário a ser removico
     * @param exchangeName nome do grupo a remover o usuário
     * @return true se adicionou, false se houve algum erro
     */
    override fun removeQueueToExchange(queueName: String, exchangeName: String): Boolean {
        try {
            doAsync { channel?.queueUnbind(queueName, exchangeName, "") }
        } catch (e: IOException) {
            return false
        }
        return true

    }


    override fun startConsumerFromQueue(queueName: String, consumer: ChatInteractor.ConsumerFromQueue) {

        val c = object : DefaultConsumer(channel) {
            override fun handleDelivery(consumerTag: String?, envelope: Envelope?, properties: AMQP.BasicProperties?, body: ByteArray?) {

                val m = body?.toString(Charset.defaultCharset())?.toMessage()
                Log.d("ChatInteractor", body?.toString(Charset.defaultCharset()))

                if (m != null) consumer.onNewMessage(m)
            }

        }

        tagConsume = channel?.basicConsume(queueName, true, c) ?: ""
    }

    override fun closeConnection() {
        doAsync {
            if (tagConsume != "") {
                channel?.basicCancel(tagConsume)
            }
        }
    }


}
