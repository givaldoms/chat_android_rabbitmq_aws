package br.net.sd.chat.main

import android.util.Log
import br.net.sd.chat.utils.toMessage
import com.rabbitmq.client.*
import org.jetbrains.anko.doAsync
import java.io.IOException
import java.nio.charset.Charset

/**
 * Created by Givaldo Marques
 * on 30/09/2017.
 */

class MainInteractorImpl : MainInteractor {

    private var connection: Connection? = null
    private var channel: Channel? = null
    private var tagConsume = ""

    init {

        doAsync {
            val factory = ConnectionFactory()
            factory.host = "ec2-52-39-124-26.us-west-2.compute.amazonaws.com"
            factory.username = "administrador"
            factory.password = "sd2017ufs"

            connection = factory.newConnection()
            channel = connection?.createChannel()
        }

    }

    /**
     * Cria uma fila no canal atual (queue == usuário)
     * @param name nome da fila a ser criada
     * @return true se pode criar, false se houve um erro
     */
    override fun createQueue(name: String): Boolean {
        try {
            doAsync {
                channel?.queueDeclare(name, true, false, false, null)
            }
        } catch (e: IOException) {
            return false
        }
        return true

    }


    /**
     * Cria um grupo do tipo fanout (todos inscritos recebem) (exchange == grupo)
     * @param exchangeName nome do grupo
     */
    override fun createExchange(exchangeName: String): Boolean {
        try {
            doAsync { channel?.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT) }
        } catch (e: AlreadyClosedException) {
            return false
        }
        return true
    }


    override fun startConsumerFromQueue(queueName: String, consumer: MainInteractor.ConsumerFromQueue) {
        doAsync {
            val c = object : DefaultConsumer(channel) {
                override fun handleDelivery(consumerTag: String?, envelope: Envelope?, properties: AMQP.BasicProperties?, body: ByteArray?) {

                    val m = body?.toString(Charset.defaultCharset())?.toMessage()
                    Log.d("MainInteractor", body?.toString(Charset.defaultCharset()))

                    if (m != null) {
                        if (m.isFromGroup()) {
                            consumer.onNewMessageFromExchange(m)
                        } else {
                            consumer.onNewMessageFromQueue(m)

                        }
                    }
                }
            }

            tagConsume = channel?.basicConsume(queueName, true, c) ?: ""
        }
    }

    override fun closeConnection() {
        doAsync {
            if (tagConsume != "") {
                channel?.basicCancel(tagConsume)
            }
        }
    }

}