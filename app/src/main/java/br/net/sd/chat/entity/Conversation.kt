package br.net.sd.chat.entity

import android.os.Parcel
import android.os.Parcelable

data class Conversation(val messages: MutableList<Message> = mutableListOf(),
                        val name: String,
                        val type: Int,
                        var hasNotification: Boolean = false) : Parcelable {




    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(Message),
            parcel.readString(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(messages)
        parcel.writeString(name)
        parcel.writeInt(type)
        parcel.writeByte(if (hasNotification) 1 else 0)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Conversation> {

        override fun createFromParcel(parcel: Parcel): Conversation = Conversation(parcel)

        override fun newArray(size: Int): Array<Conversation?> = arrayOfNulls(size)
    }

}