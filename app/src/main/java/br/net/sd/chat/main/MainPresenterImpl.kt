package br.net.sd.chat.main

import android.util.Log
import br.net.sd.chat.entity.Conversation
import br.net.sd.chat.entity.Message

/**
 * Created by Givaldo Marques
 * on 30/09/2017.
 */

class MainPresenterImpl(private val mainView: MainView? = null) : MainPresenter, MainInteractor.ConsumerFromQueue {

    var mainInteractor: MainInteractor? = null

    private val conversations = mutableListOf<Conversation>()

    val chat = 0
    val group = 1

    init {
        mainInteractor = MainInteractorImpl()

        mainView?.bindAdapter(conversations)
        mainView?.addSwipeAdapterEvent(conversations)
    }

    override fun createConversation(name: String, type: Int) {
        if (type == group) {
            mainInteractor?.createExchange(name)
        }

        conversations.add(Conversation(name = name, type = type))
    }

    override fun createGroup(name: String) {
        if (mainInteractor?.createExchange(name) == false) {
            mainView?.showToast("Erro ao criar o grupo")
        }
    }

    override fun createUser(name: String) {
        if (mainInteractor?.createQueue(name) == false) {
            mainView?.showToast("Erro ao criar o usuário")
        }
    }

    override fun getConversations(name: String) {
        mainInteractor?.createQueue(name)
        mainInteractor?.startConsumerFromQueue(name, this)
    }

    override fun onNewMessageFromQueue(message: Message) {
        var ext = true

        conversations.forEach {
            if (it.name == message.sender) {
                it.messages.add(message)
                it.hasNotification = true
                ext = false
            }
        }

        if (ext) {
            val aux = mutableListOf<Message>()
            aux.add(message)
            conversations.add(Conversation(name = message.sender, type = 0, messages = aux, hasNotification = true))
        }

        mainView?.attRecyclerView()

    }

    override fun onNewMessageFromExchange(message: Message) {
        var ext = true

        conversations.forEach {
            if (it.name == message.group) {
                it.messages.add(message)
                it.hasNotification = true
                ext = false
            }
        }

        if (ext) {
            val aux = mutableListOf<Message>()
            aux.add(message)
            conversations.add(Conversation(name = message.group?:"", type = 1, messages = aux, hasNotification = true))
        }

        mainView?.attRecyclerView()

    }

    override fun doCloseConnection() {
        mainInteractor?.closeConnection()
    }

}