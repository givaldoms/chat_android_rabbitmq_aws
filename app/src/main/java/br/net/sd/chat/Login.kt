package br.net.sd.chat

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import br.net.sd.chat.entity.User
import br.net.sd.chat.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginButton.setOnClickListener {
            usernameInputLayout.error = null

            showProgress()
            loginButton.postDelayed({
                login()
            }, 2000/*tempo de espera em ms*/)
        }
    }

    private fun login() {
        if (usernameEditText.text.isEmpty()) {
            usernameInputLayout.error = getString(R.string.field_required)
            hideProgress()
            return
        }

        val i = Intent(this, MainActivity::class.java)
        i.putExtra("user", User(usernameEditText.text.toString()))
        startActivity(i)
        finish()
    }

    private fun showProgress() {
        loginButton.text = ""
        loginProgressBar.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        loginButton.text = getString(R.string.login)
        loginProgressBar.visibility = View.INVISIBLE
    }


}
