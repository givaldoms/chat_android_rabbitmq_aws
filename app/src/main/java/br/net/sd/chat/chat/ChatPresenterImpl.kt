package br.net.sd.chat.chat

import br.net.sd.chat.entity.Conversation
import br.net.sd.chat.entity.Message
import br.net.sd.chat.entity.MessageContent
import br.net.sd.chat.entity.User
import org.jetbrains.anko.doAsync

/**
 * Created by Givaldo Marques on
 * 02/10/2017.
 */

class ChatPresenterImpl(private val conversation: Conversation,
                        private val user: User,
                        private val chatView: ChatView) : ChatPresenter, ChatInteractor.ConsumerFromQueue {

    private val chatInteractor = ChatInteractorImpl()

    init {
        chatView.startRecyclerView()
        chatView.bindAdapter(conversation.messages)
    }

    override fun getConversation() {
        val a = this
        doAsync {
            chatInteractor.startConsumerFromQueue(user.username, a)
        }
    }

    override fun onNewMessage(message: Message) {
        conversation.messages.add(message)
        chatView.attRecyclerView(conversation.messages.size)
    }

    override fun sendMessage(message: String) {
        if(message.isEmpty()) return

        val m: Message?
        val content = MessageContent(body = message)

        when {
            conversation.type == 0 -> {
                m = Message(sender = user.username, messageContent = content)
                chatInteractor.sendMessageToQueue(conversation.name, m.toJson())
            }
            conversation.type == 1 -> {
                m = Message(sender = "${conversation.name}/${user.username}",
                        messageContent = content,
                        group = conversation.name)
                chatInteractor.sendMessageExchange(conversation.name, m.toJson())

            }
        }

//        if (m != null) conversation.messages.add(m)
//
//        chatView.attRecyclerView(conversation.messages.size)

    }

    override fun addUserToGroup(username: String, groupName: String) {
        if (username.isNotEmpty() && groupName.isNotEmpty()) {
            chatInteractor.addQueueToExchange(username, groupName)
        } else {
            chatView.showToast("Parâmetros inválidos")
        }
    }

    override fun removeUserToGroup(username: String, groupName: String) {
        if (username.isNotEmpty() && groupName.isNotEmpty()) {
            chatInteractor.removeQueueToExchange(username, groupName)
        } else {
            chatView.showToast("Parâmetros inválidos")
        }
    }

    override fun doCloseConnection() {
        chatInteractor.closeConnection()
    }
}