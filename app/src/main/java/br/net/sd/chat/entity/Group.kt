package br.net.sd.chat.entity

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Givaldo Marques on 24/09/2017.
 */
data class Group(val name: String, val users: List<User>) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.createTypedArrayList(User)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeTypedList(users)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Group> {
        override fun createFromParcel(parcel: Parcel): Group = Group(parcel)

        override fun newArray(size: Int): Array<Group?> = arrayOfNulls(size)
    }
}