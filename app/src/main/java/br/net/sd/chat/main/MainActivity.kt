package br.net.sd.chat.main

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.widget.EditText
import android.widget.Toast
import br.net.sd.chat.R
import br.net.sd.chat.entity.Conversation
import br.net.sd.chat.entity.User
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), MainView {

    private lateinit var mChatsAdapter: MainAdapter
    private lateinit var mChatsRecycler: RecyclerView
    private lateinit var chatItem: Pair<Conversation, Int>
    private lateinit var user: User

    private lateinit var mainPresenter: MainPresenter

    val chat = 0
    val group = 1

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        user = intent.getParcelableExtra("user")

        initToolbar()
        startRecyclerView()

        mainFab.setOnClickListener {
            showAddDialog()
            mainFab.hide()
        }

        mainPresenter = MainPresenterImpl(this)

    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({
            mainPresenter.getConversations(user.username)
        }, 10000)
    }

    override fun onStop() {
        mainPresenter.doCloseConnection()
        super.onStop()
    }

    private fun initToolbar() {
        setSupportActionBar(this.mainToolbar)
        supportActionBar?.title = "Chat SD"
    }

    override fun startRecyclerView() {
        mChatsRecycler = findViewById(R.id.usersRecyclerView) as RecyclerView
        val ll1 = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mChatsRecycler.layoutManager = ll1

    }

    override fun bindAdapter(chats: MutableList<Conversation>) {
        mChatsAdapter = MainAdapter(chats, user, this)
        mChatsRecycler.adapter = mChatsAdapter
    }



    override fun attRecyclerView() {
        runOnUiThread {
            mChatsAdapter.notifyDataSetChanged()
        }

    }

    override fun showAddDialog() {
        val bottomSheetDialog = BottomSheetDialog(this)
        val sheetView = layoutInflater.inflate(R.layout.choose_chat_dialog, null)
        bottomSheetDialog.setContentView(sheetView)

        val llChat = sheetView.findViewById(R.id.chatLinearLayout)
        val llGroup = sheetView.findViewById(R.id.groupLinearLayout)

        llChat.setOnClickListener {
            bottomSheetDialog.dismiss()
            showDialogEditText("Nome do usuário", chat)
        }

        llGroup.setOnClickListener {
            bottomSheetDialog.dismiss()
            showDialogEditText("Nome do grupo", group)

        }

        bottomSheetDialog.setOnDismissListener {
            mainFab.show()
        }

        bottomSheetDialog.show()
    }

    private fun showDialogEditText(title: String, type: Int) {
        val editText = EditText(this)

        val dialog = AlertDialog.Builder(this)
        dialog.setPositiveButton("OK", { _, _ ->
            val t = editText.text
            if (t.isNotEmpty()) {
                mainPresenter.createConversation(t.toString(), type)
            }
        })
        dialog.setNegativeButton("Cancelar", { _, _ -> })

        dialog.setTitle(title)
        dialog.setView(editText)
        dialog.show()


    }

    override fun showSnackBar(title: String, conversations: MutableList<Conversation>) {
        Snackbar.make(mainCoordinatorLayout, title, Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.undo), { _ ->
                    conversations.add(chatItem.second, chatItem.first)
                    attRecyclerView()
                })
                .show()
    }


    override fun addSwipeAdapterEvent(conversations: MutableList<Conversation>) {
        val simpleCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView
            .ViewHolder) = false

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition

                if (direction == ItemTouchHelper.RIGHT) {

                    chatItem = Pair(conversations[position], position)
                    conversations.removeAt(position)
                    attRecyclerView()

                    if (chatItem.first.type == chat)
                        showSnackBar("Usuário removido", conversations)
                    else if (chatItem.first.type == group)
                        showSnackBar("Grupo removido", conversations)

                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(simpleCallback)
        itemTouchHelper.attachToRecyclerView(mChatsRecycler)
    }

    override fun showToast(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

}
