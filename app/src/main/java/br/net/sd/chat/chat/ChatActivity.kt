package br.net.sd.chat.chat

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import br.net.sd.chat.R
import br.net.sd.chat.entity.Conversation
import br.net.sd.chat.entity.Message
import br.net.sd.chat.entity.User
import br.net.sd.chat.utils.clear
import kotlinx.android.synthetic.main.activity_chat.*
import org.jetbrains.anko.*

class ChatActivity : AppCompatActivity(), ChatView {

    private lateinit var mChatAdapter: ChatAdapter
    private lateinit var mMessagesRecycler: RecyclerView

    private lateinit var user: User
    private lateinit var conversation: Conversation
    private lateinit var chatPresenter: ChatPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        user = intent.getParcelableExtra("user")
        conversation = intent.getParcelableExtra("conversation")


        initToolbar()

        sendImageButton.setOnClickListener {
            val m = messageEditText.text.toString()
            chatPresenter.sendMessage(m)
            messageEditText.clear()
        }

        chatPresenter = ChatPresenterImpl(conversation, user, this)
        Handler().postDelayed({
            chatPresenter.getConversation()
        }, 10000)

    }


    private fun initToolbar() {
        setSupportActionBar(this.chatToolbar)
        supportActionBar?.title = conversation.name
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    override fun onStop() {
        super.onStop()
        chatPresenter.doCloseConnection()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (conversation.type == 1) {
            menuInflater.inflate(R.menu.add_user_menu, menu)
            return true

        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.addUser -> {
                alert {
                    title = "Nome do usuário"

                    customView {
                        val a = editText()
                        a.singleLine = true

                        yesButton {
                            chatPresenter.addUserToGroup(a.text.toString(), conversation.name)
                        }

                        noButton { }


                    }

                }.show()

                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun startRecyclerView() {
        mMessagesRecycler = findViewById(R.id.chatRecyclerView) as RecyclerView
        val ll1 = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mMessagesRecycler.layoutManager = ll1
    }

    override fun bindAdapter(chats: MutableList<Message>) {
        mChatAdapter = ChatAdapter(chats, User("junior"), this)
        mMessagesRecycler.adapter = mChatAdapter
    }

    override fun attRecyclerView(sizeList: Int) {
        runOnUiThread {
            mChatAdapter.notifyDataSetChanged()
            mMessagesRecycler.smoothScrollToPosition(sizeList - 1)
        }
    }

    override fun showAddDialog() {
        val editText = EditText(this)

        val dialog = AlertDialog.Builder(this)
        dialog.setPositiveButton("OK", { _, _ ->
            val t = editText.text
            if (t.isNotEmpty()) {
                //mainPresenter.createConversation(t.toString(), type)
            }
        })
        dialog.setNegativeButton("Cancelar", { _, _ -> })

        dialog.setTitle(getString(R.string.username))
        dialog.setView(editText)
        dialog.show()
    }

    override fun showToast(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

}
