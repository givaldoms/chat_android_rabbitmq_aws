package br.net.sd.chat.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.Gson
import br.net.sd.chat.utils.getDate
import br.net.sd.chat.utils.getTime

data class Message(val sender: String,
                   val date: String = getDate(),
                   val time: String = getTime(),
                   val group: String? = null,
                   val messageContent: MessageContent) : Parcelable {


    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(MessageContent::class.java.classLoader))

    fun toJson() = Gson().toJson(this) ?: ""

    override fun toString() = "($date às $time) $sender diz: ${messageContent.body}"

    fun isFromGroup() = this.group != null

    constructor() : this(sender = "", messageContent = MessageContent(body = ""))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(sender)
        parcel.writeString(date)
        parcel.writeString(time)
        parcel.writeString(group)
        parcel.writeParcelable(messageContent, flags)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<Message> {
        override fun createFromParcel(parcel: Parcel): Message = Message(parcel)

        override fun newArray(size: Int): Array<Message?> = arrayOfNulls(size)
    }

}

class MessageContent(private val type: String = "text/plain", val body: String, val name: String? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
        parcel.writeString(body)
        parcel.writeString(name)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<MessageContent> {
        override fun createFromParcel(parcel: Parcel): MessageContent = MessageContent(parcel)

        override fun newArray(size: Int): Array<MessageContent?> = arrayOfNulls(size)
    }
}
